package hibatis.test.mapper;

import hibatis.test.entity.User;
import hibatis.BaseMapper;
import hibatis.ConflictAction;
import hibatis.FilterType;
import hibatis.annotation.*;
import hibatis.test.model.UserQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by dave on 18-7-1 下午4:35.
 */
@Mapper
public interface UserMapper extends BaseMapper<User, Long> {

    @ExecuteSelect
    List<Map> getList(UserQuery query);

    @ExecuteSelect(orderBy = "id desc")
    List<User> search(@FilterParam(field = "type") int type,
                      @FilterParam(table = "car", column = "license", type = FilterType.preMatch, join = @Join(table = "car",
                          joinColumns = @JoinColumn(column = "driver_id", referTable = "user", referColumn = "id"))) String license);

    @ExecuteSelect(filter = {@Filter(field = "type", placeholder = "1", conflict = ConflictAction.discard)})
    List<User> selectByType(@FilterParam(field = "type") Integer type);

    @ExecuteSelect(view = "detail")
    User getById(@IdParam long id);

    @ExecuteSelect(view = "detail")
    List<User> getByIds(@IdParam List<Long> ids);

    @ExecuteSelect
    List<User> searchByCondition(@FilterParam Map<String, Object> condition, @Limit int limit);

    @ExecuteInsert(columnValue = {@ColumnValue(field = "updateTime", placeholder = "now()"),
        @ColumnValue(field = "createTime", placeholder = "now()")})
    int insertList(List<User> users);

    @ExecuteInsert
    int insertMap(Map<String, Object> map);

    @ExecuteUpdate(columnValue = @ColumnValue(field = "updateTime", placeholder = "now()"))
    int changeName(@IdParam long id, @ValueParam("name") String name);

    @ExecuteUpdate(columnValue = @ColumnValue(field = "updateTime", placeholder = "now()"))
    int changeNames(@IdParam List<Long> id, @ValueParam("name") String name);

    @ExecuteUpdate
    int changeAllName(@ValueParam("name") String name);

    @ExecuteDelete(filter = @Filter(field = "type", placeholder = "1"))
    int deleteByCompanyName(@FilterParam(field = "companyName", type = FilterType.preMatch) String companyName);
}
