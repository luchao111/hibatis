package hibatis.test.model;

import hibatis.annotation.Limit;
import hibatis.annotation.Offset;

/**
 * Created by dave on 18-7-1 下午4:38.
 */
public class PagedQuery {
    @Limit
    private long pageSize = 20;

    private long pageNo = 1;

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public long getPageNo() {
        return pageNo;
    }

    public void setPageNo(long pageNo) {
        this.pageNo = pageNo;
    }

    @Offset
    public long getOffset() {
        return (pageNo - 1) * pageSize;
    }
}
