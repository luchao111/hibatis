CREATE TABLE user (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  phone VARCHAR(20) NOT NULL,
  type INT NOT NULL COMMENT '1：员工，2：老板，3：管理员',
  company_id BIGINT NOT NULL,
  boss_id BIGINT NOT NUll,
  create_time DATETIME NOT NULL DEFAULT NOW(),
  update_time DATETIME NOT NULL DEFAULT NOW()
);
INSERT INTO user (id, name, phone, type , company_id, boss_id, create_time) VALUES
  (1, '老君', '19012340000', 3, 0, 0, '2018-01-01 00:00:00'),
  (2, '孔丘', '19012340001', 3, 0, 0, '2018-01-01 01:00:00'),
  (3, '浩然', '18012340001', 1, 1, 8, '2018-06-01 01:00:00'),
  (4, '居易', '18012340002', 1, 1, 8, '2018-07-01 01:00:00'),
  (5, '韩愈', '18012340003', 1, 1, 8, '2018-07-02 01:00:00'),
  (6, '杜甫', '18012340004', 1, 1, 8, '2018-07-03 02:00:00'),
  (7, '李白', '18012340005', 1, 1, 8, '2018-08-01 03:00:00'),
  (8, '玄宗', '18012340000', 2, 1, 8, '2018-01-01 00:00:00'),
  (9, '太宗', '17012340000', 2, 2, 9, '2018-01-01 00:00:00'),
  (10, '陆游', '17012340001', 1, 2, 9, '2018-01-01 01:00:00'),
  (11, '苏轼', '17012340002', 1, 2, 9, '2018-01-02 00:00:00'),
  (12, '仲淹', '17012340003', 1, 2, 9, '2018-01-02 01:00:00'),
  (13, '安石', '17012340004', 1, 2, 9, '2018-02-02 01:00:00'),
  (14, '欧阳', '17012340005', 1, 2, 9, '2018-03-01 00:00:00'),
  (15, '天祥', '17012340006', 1, 2, 9, '2018-03-01 00:00:00');

CREATE TABLE company (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  create_user_id BIGINT NOT NULL,
  create_time DATETIME DEFAULT NOW(),
  update_user_id BIGINT NOT NULL,
  update_time DATETIME DEFAULT NOW()
);
INSERT INTO company (id, name, create_user_id, create_time, update_user_id, update_time) VALUES
  (1, '唐', 1, '2018-01-01 00:00:00', 1, '2018-01-01 00:00:00'),
  (2, '宋', 1, '2018-01-01 00:00:00', 1, '2018-01-01 00:00:00');

CREATE TABLE login_log (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  user_id BIGINT NOT NULL,
  channel VARCHAR(50) NOT NULL,
  login_at DATETIME NOT NULL,
  from_city VARCHAR(50) NOT NULL,
  source_ip VARCHAR(50) NOT NULL
);
INSERT INTO login_log (id, user_id, channel, login_at, from_city, source_ip) VALUES
  (1, 1, 'API', '2018-01-01', '北京', '127.0.0.1'),
  (2, 2, 'API', '2018-01-01', '北京', '127.0.0.1'),
  (3, 3, 'WEB', '2018-08-01', '长安', '127.0.0.1'),
  (4, 4, 'WEB', '2018-08-01', '长安', '127.0.0.1'),
  (5, 5, 'WEB', '2018-08-01', '长安', '127.0.0.1'),
  (6, 6, 'WEB', '2018-08-01', '长安', '127.0.0.1'),
  (7, 7, 'WEB', '2018-08-01', '长安', '127.0.0.1'),
  (8, 8, 'WEB', '2018-08-01', '长安', '127.0.0.1'),
  (9, 3, 'WEB', '2018-08-02', '长安', '127.0.0.1'),
  (10, 3, 'APP', '2018-08-03', '长安', '127.0.0.1'),
  (11, 3, 'APP', '2018-08-04', '长安', '127.0.0.1');

CREATE TABLE brand (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL
);
INSERT INTO brand (id, name) VALUES (1, '上汽'), (2, '比亚迪'), (3, '吉利'), (4, '长安');

CREATE TABLE model (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  brand_id BIGINT NOT NULL,
  name VARCHAR(50) NOT NULL
);
INSERT INTO model (id, brand_id, name) VALUES (1, 1, '荣威360'), (2, 1, '荣威950'), (3, 1, '荣威RX8'), (4, 2, '唐'), (5, 2, '宋'), (6, 2, '元');

CREATE TABLE car (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  license VARCHAR(50) NOT NULL,
  driver_id BIGINT DEFAULT 0,
  brand_id BIGINT NOT NULL,
  model_id BIGINT NOT NULL,
  production_date DATE NOT NULL,
  create_time DATETIME NOT NULL DEFAULT NOW()
);
INSERT INTO car(id, license, driver_id, brand_id, model_id, production_date, create_time) VALUES
  (1, 'T1001', 1, 1, 1, '2018-01-01', '2018-01-01'),
  (2, 'T1002', 2, 1, 2, '2018-01-01', '2018-01-01'),
  (3, 'T1003', 3, 2, 4, '2018-01-01', '2018-01-01'),
  (4, 'T1004', 4, 2, 5, '2018-01-01', '2018-01-01'),
  (5, 'T1005', 5, 2, 6, '2018-01-01', '2018-01-01'),
  (6, 'T1006', 6, 1, 1, '2018-01-01', '2018-01-01'),
  (7, 'T1007', 7, 1, 2, '2018-01-01', '2018-01-01'),
  (8, 'T1008', 8, 2, 4, '2018-01-01', '2018-01-01'),
  (9, 'T1009', 9, 2, 4, '2018-01-01', '2018-01-01'),
  (10, 'T1010', 10, 2, 5, '2018-01-01', '2018-01-01'),
  (11, 'T1011', 11, 2, 6, '2018-01-01', '2018-01-01'),
  (12, 'T1012', 12, 1, 1, '2018-01-01', '2018-01-01'),
  (13, 'T1013', 13, 1, 2, '2018-01-01', '2018-01-01'),
  (14, 'T1014', 14, 2, 4, '2018-01-01', '2018-01-01'),
  (15, 'T1015', 15, 2, 4, '2018-01-01', '2018-01-01'),
  (16, 'A1001', 1, 1, 1, '2018-01-01', '2018-01-01'),
  (17, 'A1002', 2, 1, 2, '2018-01-01', '2018-01-01'),
  (18, 'A1008', 8, 2, 4, '2018-01-01', '2018-01-01'),
  (19, 'A1009', 9, 2, 4, '2018-01-01', '2018-01-01');

CREATE TABLE trip (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  car_id BIGINT NOT NULL,
  driver_id BIGINT NOT NULL,
  from_city VARCHAR(50),
  to_city VARCHAR(50),
  start_time DATETIME NOT NULL,
  end_time DATETIME NULL
);
INSERT INTO trip (id, car_id, driver_id, from_city, to_city, start_time, end_time) VALUES
  (1, 1, 1, '北京', '长安', '2019-01-01 02:00:00', '2019-01-01 04:00:00'),
  (2, 16, 1, '长安', '北京', '2019-01-02 02:00:00', '2019-01-02 04:00:00'),
  (3, 1, 1, '北京', '北京', '2019-02-01 02:00:00', '2019-01-01 04:00:00'),
  (4, 16, 1, '长安', '长安', '2019-02-02 02:00:00', '2019-01-02 04:00:00');