package hibatis;

/**
 * Created by dave on 18-6-30 上午12:18.
 */
public enum JoinType {
    /** 内连接 */
    inner,

    /** 左连接 */
    left,

    /** 右连接 */
    right
}
